import argparse
import requests
import base64


def parse_args():
    usage = """ 
        usage: 
        This is a script to start client."""

    parser =  argparse.ArgumentParser(usage)

    help = "Username to authenticate to proxy"
    parser.add_argument('-u', '--user', help=help)

    help = "Password to authenticate to proxy"
    parser.add_argument('-p', '--passwd', help=help)

    help = "URL you want to connect via proxy"
    parser.add_argument('--url', default='http://www.google.com.ua', help=help)

    help = "Listening proxy port"
    parser.add_argument('--port', type=int, default=8000, help=help)

    help = "Proxy address"
    parser.add_argument('-a', '--addr', default='127.0.0.1', help=help)

    args = parser.parse_args()

    return args

options = parse_args()

proxyDict = { 
              "http"  : "http://{0}:{1}".format(options.addr, options.port)
            }

username = options.user
password = options.passwd
headers = {}
auth = base64.b64encode("%s:%s" % (username, password))
headers["Proxy-Authorization"] = ["Basic " + auth.strip()]

r = requests.get(options.url, headers=headers, proxies=proxyDict)
print "Status code: {}".format(r.status_code)



