#!/usr/bin/env python

from setuptools import setup

setup(name='proxy',
      version='1.0',
      description='HTTP auth proxy',
      author='K.G.',
      packages=['twisted.plugins',],
      install_requires=['Twisted==15.0.0', 'requests', 'MySQL-python']
     )	

