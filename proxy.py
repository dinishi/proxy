import argparse
import base64
import hashlib

from twisted.web import proxy, http
from twisted.internet import reactor
from twisted.enterprise import adbapi 

from manage_db import db_conn


def parse_args():
    usage = """ 
        usage: 
        This is a script to start proxy."""

    parser =  argparse.ArgumentParser(usage)

    help = "Listening port"
    parser.add_argument('-p', '--port', type=int, default=8000, help=help)

    help = "Bind address"
    parser.add_argument('-a', '--addr', default='127.0.0.1', help=help)

    args = parser.parse_args()

    return args
 

class ProxyRequest(proxy.ProxyRequest):        

    def process(self):
        auth_headers = base64.b64decode(self.getAllHeaders()['proxy-authorization'][2:-2].split()[1])
        user, passwd = auth_headers.split(':')
        d = db_conn.get_user(user, passwd)
        d.addCallback(self.authenticate)

    def authenticate(self, user):
        if user:
            proxy.ProxyRequest.process(self)
        else:
            self.setResponseCode(407, message='You need to authenticate')
            self.responseHeaders.addRawHeader("Content-Type", "text/html")
            self.finish()


class Proxy(proxy.Proxy):
    requestFactory = ProxyRequest

    
class ProxyFactory(http.HTTPFactory):

    def buildProtocol(self, addr):
        return Proxy()
 

def main():
    options = parse_args()
    reactor.listenTCP(options.port, ProxyFactory(), interface=options.addr)
    reactor.run()


if __name__ == '__main__':
    main()
