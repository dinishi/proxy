import argparse
import hashlib, uuid
import MySQLdb

from twisted.internet import reactor
from twisted.enterprise import adbapi 

import settings


class Connection(adbapi.ConnectionPool):

    def __reconnect(self, _method, _action, *args, **kwargs):
        conn = self.connections.get(self.threadID())
        self.disconnect(conn)
        return _method(self, _action, *args, **kwargs)
 
    def _runInteraction(self, interaction, *args, **kwargs):
        try:
            return adbapi.ConnectionPool._runInteraction(self, interaction, *args, **kwargs)
        except MySQLdb.OperationalError, e:
            print e.message
            return self.__reconnect(adbapi.ConnectionPool._runInteraction, interaction, *args, **kwargs)


class DBPool(object):
    """
        DB connection pool
    """

    _conn = None

    def __new__(cls, *args, **kwargs):
        if not cls._conn:
            cls._conn = super(DBPool, cls).__new__(cls, *args, **kwargs)
        return cls._conn

    def __init__(self):    
        self.dbpool = Connection("MySQLdb", user=settings.db_username,
                                 passwd=settings.db_password, db=settings.db_name)

    def shutdown(self):
        """
            Shutdown function
        """
        self.dbpool.close()

    def add_user(self, username, passwd):
        """
            Add user with a given username and password to db
        """
        # salt = uuid.uuid4().hex
        hashed_password = hashlib.sha512(passwd).hexdigest()
        d = self.dbpool.runQuery("INSERT INTO users VALUES(NULL, '{0}', '{1}')".format(username, hashed_password))
        return d

    def delete_user(self, username):
        """
            Delete a user with a given username from DB
        """
        d = self.dbpool.runQuery("DELETE FROM users WHERE username='{0}'".format(username))
        return d

    def get_user(self, username, passwd):
        """
            Get user with a given username and password to db
        """
        hashed_password = hashlib.sha512(passwd).hexdigest()
        d = self.dbpool.runQuery("SELECT * FROM users WHERE username='{0}' AND password='{1}'".format(username, hashed_password))
        return d



def parse_args():
    usage = """ 
        usage: 
        This is a script to add/remove users from your MySQL database.
    """

    parser =  argparse.ArgumentParser(usage)

    help = "The user to be created/deleted"
    parser.add_argument('-user', help=help)

    help = "The password of the user to be created. If no password is provided user will be deleted"
    parser.add_argument('-p','--passwd', help=help)

    args = parser.parse_args()

    return args


db_conn = DBPool()

if __name__ == '__main__':
    options = parse_args()
    if options.passwd:
        d = db_conn.add_user(options.user, options.passwd)
    else:
        d = db_conn.delete_user(options.user)

    def close(data, reactor, conn):
        conn.shutdown()
        reactor.stop()

    def fail(err, reactor):
        print err
        reactor.stop()

    d.addCallback(close, reactor, db_conn)
    d.addErrback(fail, reactor)

    reactor.run()