from twisted.application.service import IServiceMaker, Service
from twisted.internet.endpoints import clientFromString
from twisted.application import internet
from twisted.plugin import IPlugin
from twisted.python import usage

from zope.interface import implementer

from proxy import ProxyFactory
from manage_db import DBPool

class Options(usage.Options):
    optParameters = [
        ["port", "p", 8000, "The port number to listen on."],
        ["addr", "a", '127.0.0.1', "The host addr to listen on."]
    ]


@implementer(IServiceMaker, IPlugin)
class ProxyMaker(object):
    options = Options
    tapname = "proxy"
    description = "http auth proxy"

    def makeService(self, options):
        return internet.TCPServer(options['port'], ProxyFactory(), interface=options['addr'])

serviceMaker = ProxyMaker()