import argparse

def parse_args():
    usage = """ 
        usage: 
        This is a script to set your db settings."""

    parser =  argparse.ArgumentParser(usage)

    help = "The name of the DB script will connect to."
    parser.add_argument('db_name', help=help)

    help = "The username to connect to the DB"
    parser.add_argument('db_username', help=help)

    help = "The password to connect to the DB."
    parser.add_argument('db_password', help=help)

    args = parser.parse_args()

    return args

options = parse_args()

with open('settings.py', 'w') as the_file:
    the_file.write('db_name = "{0}"\n'.format(options.db_name))
    the_file.write('db_username = "{0}"\n'.format(options.db_username))
    the_file.write('db_password = "{0}"\n'.format(options.db_password))