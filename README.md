Simple http proxy using Python and Twisted.

db_setup.py - used to set up db settings. This file should be run before doing anything else. You should pass it  DB name, DB user, DB password.

manage_db.py is responsible for adding/deleting users
It works with MySQL DB which has table users with fields username, password(CHAR(128))
You should pass it username and optionally password.
If password is absent, script will delete a user with a given username, otherwise a user with given username and password will be created

proxy.py is responsible for proxying requests
You can pass optionally port and address.
It checks whether a user with credentials passed in request exists in a given DB

client.py is used to check the work of proxy
You should pass it a username and password to authenticate request to proxy, port and address of proxy and optionally a url you want to get


